use Mix.Config

# Configure your database
config :mycloud_auth, MycloudAuth.Repo,
  username: "admin",
  password: "admin",
  database: "mycloud_auth",
  hostname: "192.168.1.6",
  port: "2043",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :mycloud_auth, MycloudAuthWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
