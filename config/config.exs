# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :mycloud_auth,
  ecto_repos: [MycloudAuth.Repo]

# Configures the endpoint
config :mycloud_auth, MycloudAuthWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "l1GQ50Hbzg+iDETWqk8HYjMFMedCd4VsBUq7N+ufe5lsjzDgGlSAaFpV9O4ZuIj+",
  render_errors: [view: MycloudAuthWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: MycloudAuth.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "mgi57/zt"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
