defmodule MycloudAuth.AuthTokenTest do
  use MycloudAuth.DataCase
  alias MycloudAuth.Token

  test "salt" do
    assert "mcloud-auth" == Token.salt()
  end

  test "secret" do
    assert "Aqgnp+1QXiz6td89&LjC" == Token.secret()
  end
end


