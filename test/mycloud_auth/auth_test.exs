defmodule MycloudAuth.AuthTest do
  use MycloudAuth.DataCase

  alias MycloudAuth.Auth

  describe "users" do
    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Auth.create_user()

      user
    end

    test "register/1 returns new user id with token" do
      data = %{"username" => "test", "email" => "email", "password" => "pass"}

      assert {:ok, %{token: _, id: _}} = Auth.register(data)
    end

    test "register/1 fails with empty params" do
      data = %{}

      expected =
        {:error, :missing_data, %{email: "required", password: "required", username: "required"}}

      assert expected == Auth.register(data)
    end

    test "register/1 fails missing email" do
      data = %{"username" => "test", "password" => "pass"}

      expected = {:error, [%{email: "can't be blank"}]}

      assert expected == Auth.register(data)
    end

    test "register/1 fails missing password" do
      data = %{"username" => "test", "email" => "email"}

      expected =
        {:error,
         [
           %{username: "can't be blank"},
           %{email: "can't be blank"},
           %{password: "can't be blank"}
         ]}

      assert expected == Auth.register(data)
    end

    test "authenticate/1 succeeds returning token with correct pasword" do
      _user = user_fixture(%{"username" => "test", "email" => "email", "password" => "pass"})
      data = %{"username" => "test", "password" => "pass"}
      assert {:ok, %{token: _, id: _}} = Auth.authenticate(data)
    end

    test "authenticate/1 fails  because of wrong password" do
      _user = user_fixture(%{"username" => "test", "email" => "email", "password" => "pass"})
      data = %{"username" => "test", "password" => "passwrong"}
      assert {:error, "wrong_credentials"} = Auth.authenticate(data)
    end

    test "authenticate/1  fails because of wrong password" do
      _user = user_fixture(%{"username" => "test", "email" => "email", "password" => "pass"})
      data = %{"username" => "test1", "password" => "passwrong"}
      assert {:error, "wrong_credentials"} = Auth.authenticate(data)
    end

    test "verify_token/1  returns true" do
      _user = user_fixture(%{"username" => "test", "email" => "email", "password" => "pass"})
      data = %{"username" => "test", "password" => "pass"}
      {:ok, %{token: token}} = Auth.authenticate(data)
      assert %{valid: true } == Auth.verify_token(%{"token" => token})
    end

    test "verify_token/1  returns false" do
      assert %{valid: false} == Auth.verify_token(%{"token" => "invalid_token"})
    end

    test "authenticate fails missing credentials keys" do
      data = %{"random" => "test"}

      assert {:error, "missing_credentials"} = Auth.authenticate(data)
    end
  end
end
