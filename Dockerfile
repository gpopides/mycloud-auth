# First, download and compile all dependencies in an intermediate base image
FROM elixir:1.11 as base
# Your dependencies for the OS may differ
#RUN apk update && apk --no-cache add --virtual builds-deps build-base libressl libressl-dev build-base
WORKDIR /app
ADD mix.exs .
ADD mix.lock .
ENV MIX_ENV prod
RUN mix local.rebar --force
RUN mix local.hex --if-missing --force
RUN mix deps.get --only prod
RUN mix deps.compile

# Now build a release from our app files
FROM elixir:1.11 as intermediate
#RUN apk update && apk --no-cache add --virtual builds-deps build-base libressl libressl-dev build-base
COPY --from=base /app /app
ENV DATABASE_URL ecto://admin:admin@192.168.1.6/mycloud_auth
ENV SECRET_KEY_BASE somesecret
ENV DATABASE_PORT 2043
ENV MIX_ENV prod
RUN mix local.hex --if-missing --force
WORKDIR /app
ADD . .
RUN mix compile
RUN mix release --overwrite

# now run the release.  Make sure the alpine version below matches the alpine version
# included by erlang included by elixir:1.8-alpine
FROM bitnami/minideb:latest
RUN apt-get update && apt-get install -y openssl libncurses5-dev libc6
ENV MIX_ENV prod
ENV LANG C.UTF-8
COPY --from=intermediate /app/_build/prod/rel/prod/ /app
CMD ["/app/bin/prod", "start"]
