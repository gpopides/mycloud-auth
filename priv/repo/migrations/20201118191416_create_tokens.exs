defmodule MycloudAuth.Repo.Migrations.CreateTokens do
  use Ecto.Migration

  def change do
    create table(:tokens) do
      timestamps()
    end
  end
end
