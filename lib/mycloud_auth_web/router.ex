defmodule MycloudAuthWeb.Router do
  use MycloudAuthWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    #plug Corsica, origins: "http://localhost:3000", allow_headers: :all
    plug Corsica, origins: "http://localhost:3000", allow_headers: ["content-type"]
  end

  scope "/api" do
    pipe_through :api
    post "/login", MycloudAuthWeb.AuthController, :login
    post "/register", MycloudAuthWeb.AuthController, :register
    post "/validate_token", MycloudAuthWeb.AuthController, :validate_token
  end

end
