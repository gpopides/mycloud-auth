defmodule MycloudAuthWeb.AuthView do
  use MycloudAuthWeb, :view

  def render("register.json", %{id: user_id, token: token}) do
    %{token: token, id: user_id, success: true}
  end

  def render("register.json", %{error: reason}) do
    %{error: reason, success: false}
  end

  def render("authenticate.json", %{token: token, id: id}) do
    %{authenticated: true, token: token, id: id}
  end

  def render("authenticate.json", %{error: reason}) do
    %{authenticated: false, error: reason}
  end

  def render("missing_data.json", %{rules: rules}) do
    %{error: "invalid_data", rules: rules}
  end

  def render("validate.json", %{valid: valid}) do
    %{valid: valid}
  end
end
