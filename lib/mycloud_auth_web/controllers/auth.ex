defmodule MycloudAuthWeb.AuthController do
  use MycloudAuthWeb, :controller
  alias MycloudAuth.Auth

  def register(conn, params) do
    case Auth.register(params) do
      {:ok, result} -> render(conn, "register.json", result)
      {:error, :missing_data, rules} -> render(conn, "missing_data.json", %{rules: rules})
      {:error, reason} -> render(conn, "register.json", %{error: reason})
    end
  end

  def login(conn, params) do
    case Auth.authenticate(params) do
      {:ok, token_result} -> render(conn, "authenticate.json", token_result)
      {:error, reason} -> render(conn, "authenticate.json", %{error: reason})
    end
  end

  def validate_token(conn, params) do
    Auth.verify_token(params)
    render(conn, "validate.json", Auth.verify_token(params))
  end
end
