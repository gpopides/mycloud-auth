defmodule MycloudAuth.Auth.Token do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tokens" do
    timestamps()
  end

  @doc false
  def changeset(token, attrs) do
    token
    |> cast(attrs, [])
    |> validate_required([])
  end
end
