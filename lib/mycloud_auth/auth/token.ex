defmodule MycloudAuth.Token do
  alias Phoenix.Token, as: PhoenixToken

  def secret(), do: "Aqgnp+1QXiz6td89&LjC"
  def salt(), do: "mcloud-auth"

  @type user_id() :: String.t()
  @spec create(user_id()) :: String.t()
  def create(user_id) do
    PhoenixToken.sign(secret(), salt(), user_id)
  end

  def verify(token) do
    PhoenixToken.verify(secret(), salt(), token, max_age: 86400)
  end

end
