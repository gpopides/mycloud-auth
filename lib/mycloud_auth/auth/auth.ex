defmodule MycloudAuth.Auth do
  alias MycloudAuth.Repo
  alias MycloudAuth.Token
  alias MycloudAuth.Auth.User
  alias MycloudAuth.QueuePublisher
  # alias MycloudAuth.Auth.TokenManager

  @type data :: map()
  @type user_id :: integer()
  @type register_data :: %{
          required(:username) => String.t(),
          required(:password) => String.t(),
          required(:email) => String.t()
        }
  @type authenticate_params :: %{
          required(:username) => String.t(),
          required(:password) => String.t()
        }
  @type password_verified :: boolean()
  @type user_exists :: boolean()
  @type verify_token_params :: %{
          required(:token) => String.t()
        }

  @spec register(register_data) ::
          {:error, :missing_data,
           %{username: String.t(), password: String.t(), email: String.t()}}
          | {:ok, %{id: integer(), token: String.t()}}
          | {:error, String.t()}
          | {:error, String.t()}
  def register(register_data) when register_data == %{},
    do: {:error, :missing_data, %{username: "required", password: "required", email: "required"}}

  def register(data) do
    with {:ok, user} <- create_user(data) do
      token = Token.create(user.id)
      {:ok, %{id: user.id, token: token}}
    else
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Validates user input and returns a token if the user is authenticated
  """
  @spec authenticate(authenticate_params) ::
          {:ok, %{token: String.t()}} | {:error, String.t()} 


  def authenticate(%{"username" => username, "password" => password}) do
    case Repo.get_by(User, username: username) do
      nil ->
        authenticate_result(false)

      user ->
        Argon2.verify_pass(password, user.password)
        |> authenticate_result(user.id)
    end
  end

  def authenticate(_), do: {:error, "missing_credentials"}

  @doc """
  Checks if the token is valid (exists and not expired)
  """
  @spec verify_token(verify_token_params) :: %{valid: boolean()}
  def verify_token(%{"token" => token}) do
    Token.verify(token) |> verified_result
  end
  def verify_token(_), do: %{valid: false}

  defp verified_result({:ok, _}), do: %{valid: true}
  defp verified_result({:error, _}), do: %{valid: false}


  def create_user(attrs) do
    changeset =
      %User{}
      |> User.changeset(encrypt_password(attrs))

    case Repo.insert(changeset) do
      {:ok, user} ->
        QueuePublisher.publish(Integer.to_string(user.id), "mycloud-auth-service.new.user")
        {:ok, user}

      {:error, changeset} ->
        normalized_errors =
          changeset.errors
          |> Enum.map(fn k ->
            {attribute, {message, _}} = k
            %{attribute => message}
          end)

        {:error, normalized_errors}
    end
  end

  @spec encrypt_password(data) :: map() | %{error: String.t()}

  defp encrypt_password(data) do
    case Map.has_key?(data, "password") do
      true ->
        %{password_hash: encryped_password} = Argon2.add_hash(Map.get(data, "password"))
        %{data | "password" => encryped_password}

      false ->
        %{error: "password_not_found"}
    end
  end

  @spec authenticate_result(password_verified, user_id) ::
          {:ok, %{token: String.t(), id: integer()}} | {:error, String.t()}
  @spec authenticate_result(user_exists) :: {:error, String.t()}
  defp authenticate_result(true, user_id), do: {:ok, %{token: Token.create(user_id), id: user_id}}
  defp authenticate_result(false, _), do: {:error, "wrong_credentials"}
  defp authenticate_result(false), do: {:error, "wrong_credentials"}
end
