defmodule MycloudAuth.Repo do
  use Ecto.Repo,
    otp_app: :mycloud_auth,
    adapter: Ecto.Adapters.Postgres
end
