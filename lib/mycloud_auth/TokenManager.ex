defmodule MycloudAuth.Auth.TokenManager do
  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    {:ok, %{}}
  end

  @impl true
  def handle_call({:lookup, user_id}, _from, token_map) do
    {:reply, Map.fetch(token_map, user_id), token_map}
  end

  # def handle_cast({:create, name}, names) do
  # {:noreply}
  # end

  @impl true
  def handle_cast({:add, %{user_id: user_id, token: token}}, token_map) do
    {:noreply, Map.put(token_map, user_id, token)}
  end

  def add(user_id, token) do
    GenServer.cast(__MODULE__, {:add, %{user_id: user_id, token: token}})
  end

  def get(user_id) do
    GenServer.call(__MODULE__, {:lookup, user_id})
  end
end
