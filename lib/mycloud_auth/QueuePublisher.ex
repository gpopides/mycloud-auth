defmodule MycloudAuth.QueuePublisher do
  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
        {:ok, connection} = AMQP.Connection.open(host: "192.168.1.6")
        {:ok, channel} = AMQP.Channel.open(connection)
        {:ok, %{connection: connection, channel: channel, declared_queues: []}}
  end

  def publish(message, queue) do
    GenServer.cast(__MODULE__, {:publish,  message, queue})
  end


  @impl true
  def handle_cast({:publish, message, queue_name}, state) do
    case Enum.member?(state.declared_queues, queue_name) do
      true -> 
        publish_message(queue_name, state.channel, message)
        {:noreply, state}
      false ->  
        declare_queue(state.channel, queue_name) 

        updated_queues = [queue_name | state.declared_queues]
        {:noreply, %{state | declared_queues: updated_queues}}
    end
  end

  defp publish_message(queue_name, channel, message) do
    AMQP.Basic.publish(channel, "", queue_name, message)
  end

  defp declare_queue(channel, queue_name) do
    AMQP.Queue.declare(channel, queue_name, [durable: true])
    queue_name
  end

  @impl true
  def terminate(_reason, state) do
      AMQP.Connection.close(state.connection)
  end
end
