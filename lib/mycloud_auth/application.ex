defmodule MycloudAuth.Application do
  require Logger
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      MycloudAuth.Repo,
      # Start the endpoint when the application starts
      MycloudAuthWeb.Endpoint,
      # Starts a worker by calling: MycloudAuth.Worker.start_link(arg)
      # {MycloudAuth.Worker, arg},
      {MycloudAuth.Auth.TokenManager, name: MycloudAuth.Auth.TokenManager},
      {MycloudAuth.QueuePublisher, name: MycloudAuth.QueuePublisher},
    ]

    register_service()

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MycloudAuth.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MycloudAuthWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp register_service do
    if Mix.env != :test do
      paths = MycloudAuthWeb.Router.__routes__ 
              |> Enum.map(fn route -> String.slice(route.path, 1..-1) end)  # return path without leading slash
      body = %{
        host: "http://localhost:4000",
        paths: paths,
        name: "authentication-service"
      }

      registry_url = "http://localhost:4001/register"
      headers = [{"Content-Type", "application/json"}]

      with {:ok, _response} <- HTTPoison.post(registry_url, Jason.encode!(body), headers) do
        Logger.info("Service registered to service registry")
      else
        {:error, _reason} -> Logger.error("Could not register service")
      end
    end
  end
end
